import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../constants/colors'

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
    },
    screen: {
        height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    imageConatiner: {
        flex: 0.90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        height: 80,
        width: Dimensions.get('window').width - (Dimensions.get('window').width / 2)
    },
    inputContainer: {
        marginTop: 30
    },
    inputField: {
        width: Dimensions.get('window').width - 60,
        backgroundColor: 'white',
        margin: 10,
        padding: 15,
        fontSize: 18
    },
    bottonContainer: {
        flex: 0.10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: '100%',
        alignItems: 'center'
    },
    poweredBy: {
        color: 'white',
        fontSize: 16,
        padding: 20
    }
})

export default styles