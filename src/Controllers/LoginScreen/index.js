import React from 'react'
import { View, Text, Image, TextInput, ScrollView, StatusBar, Platform } from 'react-native'
import styles from './style'
import AuthorisedUserNavigationStack from '../../Navigation/AuthorisedUserNavigationStack'
// import LongButton from '../../CustomComponent/Buttons/LongButton'

import Realm from 'realm';
import { useState } from 'react';
let realm;

import userDetails from '../../schema/userDetails'
import LongButton from '../../CustomComponent/Buttons/LongButton';
import StatusBarSetup from '../../CustomComponent/StatusBarSetup';
const UserloginViewController = props => {

    const [isValidUser, setValidUser] = useState(false)


    realm = new Realm({
        path: 'UserDatabase.realm',
        schema: [userDetails],
    });



    const saveUserStatus = async () => {
        try {
            realm.write(() => {
                realm.create('userDetails', {
                    isValid: 1
                });
            });
        } catch (error) {
            throw error
        }
        setValidUser(true)
    }
    if (isValidUser) {
        return (<AuthorisedUserNavigationStack />);
    }

    return (
        <ScrollView style={styles.scroll}>
            <StatusBarSetup />
            <View style={styles.screen}>
                <View style={styles.imageConatiner}>
                    <Image
                        style={styles.logo}
                        source={require('../../assets/images/abc-logo-white.png')}
                    />

                    <View style={styles.inputContainer}>
                        <TextInput
                            placeholder="User name"
                            style={styles.inputField}
                        />

                        <TextInput
                            placeholder="Password"
                            style={styles.inputField}
                        />
                    </View>

                    <LongButton title="Sign In" onPress={saveUserStatus} />

                </View>

                <View style={styles.bottonContainer}>
                    <Text style={styles.poweredBy}>Powered by ABC</Text>
                </View>
            </View>
        </ScrollView>
    )
}

export default UserloginViewController