import React from 'react'
import { Platform, SafeAreaView, StatusBar, View, FlatList, Text } from 'react-native'
import AccountDetailsView from '../../CustomComponent/AccountDetailsView'
import HeaderView from '../../CustomComponent/Header/Header'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'
import styles from './style'


const ReceivedStockViewController = props => {
    const Accounts = [
        {
            id: 1,
            name: 'IBAN Account',
            source: require('../../assets/images/round-ETH.png'),
            currentValue: '-$ 0,25',
            totalAmount: '$ 1,645,64'
        },
        {
            id: 2,
            name: 'BTC Account',
            source: require('../../assets/images/round-bitcoin.png'),
            currentValue: '-$ 0,25',
            totalAmount: '$ 1,645,64'
        },
        {
            id: 3,
            name: 'ETH Account',
            source: require('../../assets/images/round-ETH.png'),
            currentValue: '-$ 0,25',
            totalAmount: '$ 1,645,64'
        }
    ]

    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <HeaderView />
            <View style={styles.secondaryView}>
                <FlatList
                    data={Accounts}
                    keyExtractor={item => item.id}
                    renderItem={itemData =>
                        <AccountDetailsView
                            source={itemData.item.source}
                            accountType={itemData.item.name}
                            totalBalance={itemData.item.totalAmount}
                            currentValue={itemData.item.currentValue} />
                    }
                    ListFooterComponent={
                        <View>
                            <View style={styles.totalAmountView}>
                                <Text style={styles.lblTotalAmount}>Total Balance</Text>
                                <Text style={styles.totalAmountValue}>$10.890,90</Text>
                            </View>
                            <Text style={styles.seprator}></Text>
                        </View>
                    }
                />



            </View>
        </SafeAreaView>
    )
}


export default ReceivedStockViewController