import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'black'
    },
    secondaryView: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    seprator: {
        backgroundColor: 'lightgrey',
        width: '100%',
        height: 0.5
    },
    totalAmountView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        width: '100%',
        height: 80
    },
    lblTotalAmount: {
        marginLeft: 20,
        fontSize: 22,
        color: '#6e6e6e'
    },
    totalAmountValue: {
        color: 'black',
        fontSize: 20,
        fontWeight: '600'
    },
    seprator: {
        backgroundColor: 'lightgrey',
        width: '100%',
        height: 0.5
    },

})

export default styles