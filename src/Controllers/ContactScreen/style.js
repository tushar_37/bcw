import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'black'
    },
    secondaryView: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TouchableOpacityStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 20,
    },

    FloatingButton: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
    }
})

export default styles