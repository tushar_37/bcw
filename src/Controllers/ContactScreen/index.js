import React from 'react'
import { View, TouchableOpacity, Image, SafeAreaView, StatusBar, FlatList } from 'react-native'
import styles from './style'
import HeaderView from '../../CustomComponent/Header/Header'
import ContactDetailsView from '../../CustomComponent/ContactDetailsView'
import FloatingAddButton from '../../CustomComponent/Buttons/FloatingAddButton'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'

const ContactViewController = props => {

    const Contacts = ['Markus Steck', 'Mark Wood', 'Alex', 'Giampiero Marghella']

    // functions

    const didSelectAddContact = () => {
        props.navigation.navigate('Add Contact')
    }

    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <View>
                <HeaderView showPriceView={false} />
            </View>

            <View style={styles.secondaryView}>

                <FlatList
                    data={Contacts}
                    renderItem={itemData =>
                        <ContactDetailsView name={itemData.item} />
                    } />

            </View>

            <FloatingAddButton onPress={didSelectAddContact} />

        </SafeAreaView>
    )
}

export default ContactViewController