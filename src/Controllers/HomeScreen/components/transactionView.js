import React from 'react'
import { View, Dimensions, StyleSheet } from 'react-native'

import TransactionButtonView from '../../../CustomComponent/Buttons/TransactionButtonView'


const TransactionsView = ({ onPressBuy, onPressReceive, onPressSend }) => {
    return (
        <View style={styles.container}>
            <TransactionButtonView title="Send" onPress={onPressSend} />
            <TransactionButtonView title="Receive" onPress={onPressReceive} />
            <TransactionButtonView title="Buy" onPress={onPressBuy} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 0.1,
        flexDirection: 'row',
        backgroundColor: 'white',
        width: Dimensions.get('window').width,
        justifyContent: "space-between",
        alignItems: 'center',
        padding: 10,
        margin: 5
    }
})

export default TransactionsView