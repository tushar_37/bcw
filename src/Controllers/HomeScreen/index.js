import React from 'react'
import { View, SafeAreaView, Platform, StatusBar, FlatList } from 'react-native'

import HeaderView from '../../CustomComponent/Header/Header'
import styles from './style'
import TransactionsView from './components/transactionView'

import StockUpdateView from '../../CustomComponent/StockUpdateView'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'

const HomeViewController = props => {
    const Contacts = [{
        id: 1,
        name: 'Markus Steck',
        date: '23 June 2020',
        currentValue: '-$ 0,25',
        totalAmount: '$ 1,645,64'
    },
    {
        id: 2,
        name: 'Giampiero',
        date: '22 June 2020',
        currentValue: '-$ 0,25',
        totalAmount: '$ 1,645,64'
    },
    {
        id: 3,
        name: 'Alex',
        date: '22 June 2020',
        currentValue: '-$ 0,25',
        totalAmount: '$ 1,645,64'
    },
    {
        id: 4,
        name: '1F1tAz5...GNn4xqX',
        date: '22 June 2020',
        currentValue: '-$ 0,25',
        totalAmount: '$ 1,645,64'
    }, {
        id: 5,
        name: 'Alex',
        date: '27 June 2020',
        currentValue: '-$ 0,25',
        totalAmount: '$ 1,645,64'
    }]
    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <View>
                <HeaderView />
            </View>

            <View style={styles.secondaryView}>

                <TransactionsView
                    onPressBuy={() => { props.navigation.navigate('Buy Stock') }}
                    onPressReceive={() => { props.navigation.navigate('Received') }}
                    onPressSend={() => { props.navigation.navigate('Send') }}
                />

                <View style={{ flex: 1 }}>
                    <FlatList
                        data={Contacts}
                        keyExtractor={item => item.id}
                        renderItem={itemData =>
                            <StockUpdateView
                                name={itemData.item.name}
                                date={itemData.item.date}
                                currentValue={itemData.item.currentValue}
                                totalAmount={itemData.item.totalAmount} />
                        } />
                </View>

            </View>
        </SafeAreaView>
    )
}

export default HomeViewController