import React, { useState } from 'react'
import { View, Text, Image } from 'react-native'
import styles from './style'
import AuthorisedUserNavigationStack from '../../Navigation/AuthorisedUserNavigationStack'


import Realm from 'realm';
import AuthNavigationStack from '../../Navigation/AuthNavigationStack'
let realm;
import userDetails from '../../schema/userDetails'
const SplashScreenViewController = props => {

    const [isTimeOver, setTimeOver] = useState(false)

    setTimeout(async () => {
        setTimeOver(true)
    }, 2000);

    if (isTimeOver) {
        try {
            realm = new Realm({ path: 'UserDatabase.realm', schema: [userDetails], });
            var user_details = realm.objects('userDetails');
            console.log('USERS values::::', user_details)
            if (user_details.length > 0) {
                for (let user of user_details) {
                    if (user.isValid == 1) {
                        return (<AuthorisedUserNavigationStack />);
                    } else {
                        return (<AuthNavigationStack />)
                    }
                }
            } else {
                return (<AuthNavigationStack />)
            }
        } catch (exception) {
            throw exception
        }
    }

    return (
        <View style={styles.screen}>
            <View style={styles.imageConatiner}>
                <Image
                    style={styles.logo}
                    source={require('../../assets/images/abc-logo-white.png')}
                />
            </View>
            <View style={styles.bottonContainer}>
                <Text style={styles.poweredBy}>Powered by ABC</Text>
            </View>
        </View>
    )
}

export default SplashScreenViewController