import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    imageConatiner: {
        flex: 0.90,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        height: 80,
        width: Dimensions.get('window').width - (Dimensions.get('window').width / 2)
    },
    bottonContainer: {
        flex: 0.10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: '100%',
        alignItems: 'center'
    },
    poweredBy: {
        color: 'white',
        fontSize: 14,
        padding: 20
    }
})

export default styles