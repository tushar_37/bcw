import React from 'react'
import { SafeAreaView, View, Platform, ScrollView, Text, TextInput, TouchableOpacity, Image } from 'react-native'
import LongButton from '../../CustomComponent/Buttons/LongButton'
import HeaderView from '../../CustomComponent/Header/Header'
import Icon from 'react-native-vector-icons/FontAwesome5'
import styles from './style'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'
import ToInputQRView from '../../CustomComponent/ToInputQRView'
import InputQRView from '../../CustomComponent/InputQRView'


const BuyStockViewController = props => {

    const SelectMethodViewLbl = ({ title }) => {
        return (
            <View style={styles.chooseLbl}>
                <Text style={{ fontSize: 18 }}>{title}</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <View>
                <HeaderView />
            </View>

            <View style={styles.secondaryView}>
                <ScrollView>
                    <View style={styles.inputContainer}>

                        <SelectMethodViewLbl title="Pay from account" />

                        <TextInput
                            placeholder="Select"
                            style={styles.inputField}
                        />
                        <View style={styles.dropDownArrowView}>
                            <Icon name="caret-down" color='grey' size={15} />
                        </View>
                    </View>

                    <View style={styles.inputContainer}>
                        <SelectMethodViewLbl title="Purchase amount" />


                        <InputQRView
                            placeholder="Amount"
                            source={require('../../assets/images/bitcoin.png')} />

                        <Text style={styles.ORlbl}>OR</Text>

                        <InputQRView
                            placeholder="Amount"
                            source={require('../../assets/images/Eurp.png')} />
                    </View>

                    <View style={styles.stockInformationView}>
                        <Text>Exchange rate : 1 BTC = 6.996 Euro  </Text>
                    </View>

                    <LongButton
                        leaveSapace={40}
                        title="Buy"
                        onPress={() => {
                            props.navigation.goBack()
                        }} />

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default BuyStockViewController