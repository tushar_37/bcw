import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../constants/colors'

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'black'
    },
    secondaryView: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    addContactLbl:{ margin: 10, fontSize: 16 },
    inputField: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        width: Dimensions.get('window').width - 40,
        height: 50,
        backgroundColor: 'white',
        margin: 5,
        padding: 10,
        fontSize: 18
    },
    inputQRContainer: {
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 40
    },
    inputFieldWithQR: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        height: 50,
        backgroundColor: 'white',
        fontSize: 18,
        width: Dimensions.get('window').width - 90,
        padding: 10,
    },
    signInContainer: {
        width: Dimensions.get('window').width - 40,
        height: 45,
        margin: 10,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    signIn: {
        fontSize: 20,
        color: 'white'
    },
})

export default styles