import React from 'react'
import { View, SafeAreaView, StatusBar, Text, TextInput, TouchableOpacity, ScrollView, Image } from 'react-native'
import styles from './style'
import HeaderView from '../../CustomComponent/Header/Header'
import LongButton from '../../CustomComponent/Buttons/LongButton/index'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'
const AddContactViewController = props => {

    const InputQRView = ({ placeholder, onPress }) => {
        return (
            <View style={styles.inputQRContainer}>
                <TextInput
                    placeholder={placeholder}
                    style={styles.inputFieldWithQR}
                />
                <TouchableOpacity
                    onPress={onPress}>
                    <Image
                        style={{ height: 50, width: 50 }}
                        source={require('../../assets/images/QR.png')}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <View>
                <HeaderView showPriceView={false} />
            </View>

            <View style={styles.secondaryView}>
                <ScrollView>
                    <View style={styles.inputContainer}>
                        <Text style={styles.addContactLbl}>Add New Contact</Text>
                        <TextInput
                            placeholder="Full Name"
                            style={styles.inputField}
                        />

                        <TextInput
                            placeholder="Email"
                            style={styles.inputField}
                        />

                        <TextInput
                            placeholder="Mobile Nr"
                            style={styles.inputField}
                        />

                    </View>

                    <View style={styles.inputContainer}>

                        <InputQRView placeholder="IBAN Address" />

                        <InputQRView placeholder="BTC Address" />

                        <InputQRView placeholder="ETH Address" />

                    </View>

                    <LongButton
                        leaveSapace={40}
                        title="Save"
                        onPress={() => {
                            props.navigation.goBack()
                        }} />

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default AddContactViewController