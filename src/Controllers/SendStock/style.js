import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../constants/colors'

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'black'
    },
    secondaryView: {
        flex: 1,
        width: Dimensions.get('window').width,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },

    inputContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputField: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        width: Dimensions.get('window').width - 40,
        height: 50,
        backgroundColor: 'white',
        margin: 5,
        padding: 10,
        fontSize: 18
    },
    inputQRContainer: {
        margin: 5,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 40
    },
    inputFieldWithQR: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        height: 50,
        backgroundColor: 'white',
        fontSize: 18,
        width: Dimensions.get('window').width - 90,
        padding: 10,
    },

    stockInformationView: { justifyContent: 'center', alignItems: 'center', margin: 20 },
    ORlbl: { margin: 10, fontSize: 16 },
    chooseLbl: { justifyContent: 'flex-start', width: '95%' },
    textFieldView: {
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 40
    },
    textField: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        height: 50,
        backgroundColor: 'white',
        fontSize: 18,
        width: Dimensions.get('window').width - 40,
        padding: 10,
        marginTop: 5,
        marginBottom: 20
    }

})

export default styles