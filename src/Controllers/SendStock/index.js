import React from 'react'
import { SafeAreaView, View, ScrollView, Text, TextInput, TouchableOpacity, Image } from 'react-native'
import LongButton from '../../CustomComponent/Buttons/LongButton'
import HeaderView from '../../CustomComponent/Header/Header'
import styles from './style'
import StatusBarSetup from '../../CustomComponent/StatusBarSetup'
import ToInputQRView from '../../CustomComponent/ToInputQRView'
import InputQRView from '../../CustomComponent/InputQRView'


const SendStockViewController = props => {

    return (
        <SafeAreaView style={styles.screen}>
            <StatusBarSetup />
            <View>
                <HeaderView />
            </View>

            <View style={styles.secondaryView}>
                <ScrollView>
                    <ToInputQRView placeholder="Send To Address" />
                    <View style={styles.inputContainer}>

                        <View style={styles.chooseLbl}>
                            <Text style={{ fontSize: 18 }}>Amount</Text>
                        </View>

                        <InputQRView
                            placeholder="Amount"
                            source={require('../../assets/images/bitcoin.png')} />

                        <Text style={styles.ORlbl}>OR</Text>

                        <InputQRView
                            placeholder="Amount"
                            source={require('../../assets/images/Eurp.png')} />


                        <View style={styles.textFieldView}>
                            <TextInput
                                style={styles.textField}
                                placeholder="What's this for?"
                            />
                        </View>

                    </View>

                    <LongButton
                        leaveSapace={40}
                        title="Send"
                        onPress={() => {
                            props.navigation.goBack()
                        }} />

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default SendStockViewController