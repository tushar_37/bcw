import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import ContactViewController from '../Controllers/ContactScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5'
import ContactNavigationStack from './ContactNavigationStack'
import { Colors } from '../constants/colors';
import { StyleSheet } from 'react-native';
import HomeNavigationStack from './HomeNavigationStack';


const Tab = createBottomTabNavigator();

const AuthorisedUserNavigationStack = props => {

    return (
        < NavigationContainer independent={true}>
            <Tab.Navigator
                tabBarOptions={{
                    style: {
                        backgroundColor: 'black',
                    },
                    activeBackgroundColor: Colors.primary,
                    labelStyle: styles.lbl,
                }
                }
            >
                <Tab.Screen
                    name="Home"
                    component={HomeNavigationStack}
                    options={{
                        tabBarIcon: () => (
                            < Icon name="home" color='white' size={25} />
                        )
                    }}
                />
                <Tab.Screen
                    name="Contact"
                    component={ContactNavigationStack}
                    options={{
                        tabBarIcon: () => (
                            < Icon name="address-book" color='white' size={25} solid />
                        )
                    }}
                />

                <Tab.Screen
                    name="Alerts"
                    component={ContactViewController}
                    options={{
                        tabBarIcon: () => (
                            < Icon name="bell" color='white' size={25} solid />
                        )
                    }}
                />

                <Tab.Screen
                    name="Settings"
                    component={ContactViewController}
                    options={{
                        tabBarIcon: () => (
                            < Icon name="cog" color='white' size={25} />
                        )
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer >
    )
}

const styles = StyleSheet.create({
    lbl: {
        color: 'white',
        fontSize: 12,
        fontWeight: '600'
    }
})
export default AuthorisedUserNavigationStack

//https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae