import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import UserloginViewController from '../Controllers/LoginScreen'

const Stack = createStackNavigator()

const AuthNavigationStack = props => {
    return (
        < NavigationContainer independent={true}>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen
                    name="Login"
                    component={UserloginViewController} />
            </Stack.Navigator>
        </NavigationContainer >
    )
}

export default AuthNavigationStack

//https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae