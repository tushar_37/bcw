import React from 'react'
import ContactViewController from '../Controllers/ContactScreen'
import AddContactViewController from '../Controllers/AddContact'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
const Stack = createStackNavigator()

const ContactNavigationStack = props => {
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen
                    name="Alerts"
                    component={ContactViewController}
                />
                <Stack.Screen
                    name="Add Contact"
                    component={AddContactViewController}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


export default ContactNavigationStack