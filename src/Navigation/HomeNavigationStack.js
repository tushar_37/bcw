import React from 'react'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import BuyStockViewController from '../Controllers/BuyStock'
import HomeViewController from '../Controllers/HomeScreen'
import ReceivedStockViewController from '../Controllers/RceivedStock'
import SendStockViewController from '../Controllers/SendStock'

const Stack = createStackNavigator()

const HomeNavigationStack = props => {
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen
                    name="Home"
                    component={HomeViewController}
                />
                <Stack.Screen
                    name="Buy Stock"
                    component={BuyStockViewController}
                />
                <Stack.Screen
                    name="Received"
                    component={ReceivedStockViewController}
                />
                <Stack.Screen
                    name="Send"
                    component={SendStockViewController}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


export default HomeNavigationStack