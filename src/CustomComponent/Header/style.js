import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'black',
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageConatiner: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        height: 50,
        width: 100
    },
    priceViewSetup: {
        marginTop: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    holdingLbl: {
        // margin: 5,
        color: '#909090'
    },
    priceLbl: {
        // margin: 5,
        color: 'white',
        fontSize: 28
    },
    totalPrice: {
        // margin: 5,
        color: '#e1e1e1'
    },
    bottonContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: '100%',
        alignItems: 'center'
    },
    poweredBy: {
        color: 'white',
        fontSize: 12,
        paddingRight: 10,
        paddingBottom: 5
    }
})

export default styles