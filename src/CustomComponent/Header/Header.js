import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './style'

const HeaderView = ({ price, totalPrice, showPriceView = true }) => {
    return (
        <View style={styles.header} >
            <Image
                style={styles.logo}
                source={require('../../assets/images/abc-logo-white.png')}
            />

            {showPriceView && <View style={styles.priceViewSetup}>
                <Text
                    style={styles.holdingLbl}
                >Current Holding</Text>
                <Text
                    style={styles.priceLbl}
                >$ 1,35</Text>
                <Text
                    style={styles.totalPrice}
                >($ 8,860.28)</Text>
            </View>}

            <View style={styles.bottonContainer}>
                <Text style={styles.poweredBy}>Powered by ABC</Text>
            </View>


        </View>
    )
}


export default HeaderView