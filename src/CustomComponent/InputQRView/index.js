import React from 'react'
import { View, TextInput, TouchableOpacity, Image } from 'react-native'
import styles from './style'
const InputQRView = ({ placeholder, onPress, source }) => {
    return (
        <View style={styles.inputQRContainer}>
            <TextInput
                placeholder={placeholder}
                style={styles.inputFieldWithQR}
            />
            <TouchableOpacity
                onPress={onPress}>
                <Image
                    style={styles.image}
                    source={source}
                />
            </TouchableOpacity>
        </View>
    )
}

export default InputQRView