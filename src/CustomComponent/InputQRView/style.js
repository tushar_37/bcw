import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({

    inputQRContainer: {
        margin: 5,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 40
    },
    inputFieldWithQR: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        height: 50,
        backgroundColor: 'white',
        fontSize: 18,
        width: Dimensions.get('window').width - 90,
        padding: 10,
    },
    image:{ height: 50, width: 50 }

    
})

export default styles