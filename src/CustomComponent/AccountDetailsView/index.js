import React from 'react'
import { Image, View, Text } from 'react-native'
import styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome5'

const AccountDetailsView = ({ source, accountType, totalBalance, currentValue }) => {
    return (
        <View>
            <Text style={styles.seprator}></Text>

            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image
                        style={styles.imageLayout}
                        source={source} />
                </View>
                <View style={styles.accountTypeContainer}>
                    <Text style={styles.accountType}>{accountType}</Text>
                </View>

                <View style={styles.accountBalanceSection}>
                    <View style={styles.balanceInfo}>
                        <Text style={styles.currentAmount}>{currentValue}</Text>
                        <Text style={styles.totalAmount}>{totalBalance}</Text>
                    </View>
                    <Icon name="chevron-right" size={20} color='grey' />
                </View>
            </View>

            <Text style={styles.seprator}></Text>
        </View>
    )
}

export default AccountDetailsView