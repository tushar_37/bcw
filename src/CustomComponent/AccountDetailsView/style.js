import { Dimensions, StyleSheet } from "react-native";


const styles = StyleSheet.create({
    seprator: {
        backgroundColor: 'lightgrey',
        width: '100%',
        height: 0.5
    },
    container: {
        // flex:1,
        width: Dimensions.get('window').width,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    imageContainer: {
        flex: 0.15,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },
    imageLayout: { width: 60, height: 60 },
    accountTypeContainer: {
        flex: 0.50
    },
    accountType: {
        fontSize: 22,
        color: '#6e6e6e'
    },
    accountBalanceSection: {
        flex: 0.35,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        margin: 10
    },
    balanceInfo: {
        flex: 0.9
    },
    currentAmount: {
        fontSize: 20,
        color: 'black',
    },
    totalAmount: {
        fontSize: 14,
        color: '#6e6e6e'
    }

})


export default styles