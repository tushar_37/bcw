import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../../constants/colors'

const styles = StyleSheet.create({
    container: {
      //  width: Dimensions.get('window').width - 60,
        height: 45,
        margin: 10,
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleLayout: {
        fontSize: 20,
        color: 'white'
    },
})


export default styles
