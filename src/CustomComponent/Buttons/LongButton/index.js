import React from 'react'
import { TouchableOpacity, Text, Dimensions } from 'react-native'
import styles from './style'

const LongButton = ({ title, onPress, leaveSapace = 60 }) => {
    return (
        <TouchableOpacity
            style={{ ...styles.container, width: Dimensions.get('window').width - leaveSapace }}
            onPress={onPress}
        >
            <Text style={styles.titleLayout}>{title}</Text>

        </TouchableOpacity>
    )
}

export default LongButton