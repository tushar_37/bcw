import { StyleSheet } from 'react-native'
import { Colors } from '../../../constants/colors'

const styles = StyleSheet.create({
    actionBtn: {
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
        height: '85%',
        width: '30%'
    },
    lbl: {
        fontSize: 20,
        color: 'white'
    }
})

export default styles