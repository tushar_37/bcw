import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styles from './style'

const TransactionButtonView = ({ title, onPress }) => {
    return (
        <TouchableOpacity
            style={styles.actionBtn}
            onPress={onPress}>
            <Text style={styles.lbl}>{title}</Text>
        </TouchableOpacity>
    )
}

export default TransactionButtonView