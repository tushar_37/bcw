import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    TouchableOpacityStyle: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 20,
    },

    FloatingButton: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
    }
})

export default styles