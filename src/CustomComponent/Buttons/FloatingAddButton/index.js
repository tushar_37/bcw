import React from 'react'
import { TouchableOpacity, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Colors } from '../../../constants/colors'
import styles from './style'

const FloatingAddButton = ({ onPress }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={styles.TouchableOpacityStyle}
            onPress={onPress} >

            <Icon name="plus-circle" color={Colors.primary} size={55} />

        </TouchableOpacity>
    )
}

export default FloatingAddButton