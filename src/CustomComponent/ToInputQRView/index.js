import React from 'react'
import { View, Image, TextInput, TouchableOpacity } from 'react-native'
import styles from './style'

const ToInputQRView = ({ placeholder, onPress }) => {
    return (
        <View style={styles.container}>

            <TouchableOpacity
                onPress={onPress}>
                <Image
                    style={styles.imageSize}
                    source={require('../../assets/images/To.png')}
                />
            </TouchableOpacity>

            <TextInput
                placeholder={placeholder}
                style={styles.input}
            />

            <TouchableOpacity
                onPress={onPress}>
                <Image
                    style={styles.imageSize}
                    source={require('../../assets/images/QR.png')}
                />
            </TouchableOpacity>


        </View>
    )
}

export default ToInputQRView