import { StyleSheet, Dimensions } from "react-native";


const styles = StyleSheet.create({
    container: {
        margin: 5,
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 40
    },
    imageSize: {
        height: 50,
        width: 50
    },
    input: {
        borderWidth: 1,
        borderColor: 'lightgrey',
        height: 50,
        fontSize: 18,
        padding: 10,
        flex: 1
    }

})

export default styles