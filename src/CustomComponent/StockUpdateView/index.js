import React from 'react'
import { View, Text, Image, TouchableOpacity } from "react-native"
import styles from './style'


const StockUpdateView = ({ name, date, currentValue, totalAmount }) => {
    return (
        <View>
            <Text style={styles.seprator}></Text>

            <View style={styles.container}>
                <View style={styles.userDetailView}>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.date}>{date}</Text>
                </View>

                <View style={styles.userStockDetailView}>
                    <Text style={styles.currentValue}>{currentValue}</Text>
                    <Text style={styles.totalAmount}>{totalAmount}</Text>
                </View>

            </View >
            <Text style={styles.seprator}></Text>
        </View>
    )
}


export default StockUpdateView



// TODO: Image component seprate with action