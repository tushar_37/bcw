import { Dimensions, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    seprator: { backgroundColor: 'lightgrey', width: '100%', height: 0.5 },
    container: {
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        height: 65,
        backgroundColor: 'white'
    },
    userDetailView: {
        flex: 0.70,
        padding: 15
    },
    userStockDetailView: {
        flex: 0.30,
        padding: 15
    },
    name: {
        fontSize: 20
    },
    date: {
        fontSize: 13,
        color: 'grey'
    },
    currentValue: {
        fontSize: 20,
        textAlign: 'right'
    },
    totalAmount: {
        fontSize: 13,
        color: 'grey',
        textAlign: 'right'
    }


})


export default styles