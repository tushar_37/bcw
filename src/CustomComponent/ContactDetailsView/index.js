import React from 'react'
import { View, Text, TouchableOpacity } from "react-native"
import styles from './style'
import Icon from 'react-native-vector-icons/FontAwesome5'

const ContactDetailsView = ({ name }) => {

    return (
        <View style={styles.container}>

            <TouchableOpacity style={styles.actionConainer}>
                <Icon name="chevron-down" size={20} />
            </TouchableOpacity>

            <View style={styles.nameContainer}>
                <Text style={styles.name}>{name}</Text>
            </View>

            <TouchableOpacity style={styles.actionConainer}>
                <Icon name="pencil-alt" size={20} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.actionConainer}>
                <Icon name="trash-alt" size={20} />
            </TouchableOpacity>

        </View >
    )
}


export default ContactDetailsView



// TODO: Image component seprate with action