import { Dimensions, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        marginTop: 1,
        marginBottom: 1,
        flexDirection: 'row',
        width: Dimensions.get('window').width,
        backgroundColor: '#ededed',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60
    },
    nameContainer: {
        flex: 0.70,
        justifyContent: 'center',
    },
    name:{ paddingLeft: 20, fontSize: 18 },
    actionConainer: {
        flex: 0.10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    arrow: {
        width: 15,
        height: 15
    },
    edit: {
        width: 20,
        height: 20
    },
    delete: {
        width: 20,
        height: 20
    }
})


export default styles