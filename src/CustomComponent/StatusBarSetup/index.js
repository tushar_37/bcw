import React from 'react'
import { Platform, StatusBar } from 'react-native'

const StatusBarSetup = () => {
    return (
        Platform.OS === 'ios' ? null : <StatusBar backgroundColor="black" />
    )
}

export default StatusBarSetup