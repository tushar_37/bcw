/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import SplashScreenViewController from './src/Controllers/SplashScreen';
const App = () => {

  return (<SplashScreenViewController />)

};
export default App;
